package com.prueba.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.prueba.dao.GenericDao;
import com.prueba.entity.Demo;

@Service
public class DemoService implements GenericDao<Demo, Integer>{
	
	@Autowired
	RestTemplate clienteRest;
	
	@Value("${path.server}")
	private String server;

	@Override
	public Demo get(Integer id) {
		try {
			Map<String, Integer> variableMap = new HashMap<String, Integer>();
			variableMap.put("id", id);
			Demo demo = clienteRest.getForObject(server + "/api/v1/demoServ/demo/get/{id}", Demo.class, variableMap);
			if(demo!=null) {
				return demo;
			}else {
				return new Demo();
			}
		}catch(HttpStatusCodeException  e) {
			System.out.println("Codigo: " + e.getStatusCode());
			System.out.println("Error: " + e.getMessage());
			e.printStackTrace();
			return null;	
		}
	}

	@Override
	public Demo save(Demo demo) {
		try {
			HttpEntity<Demo> body = new HttpEntity<Demo>(demo);
			ResponseEntity<Demo> response = clienteRest.exchange(server + "/api/v1/demoServ/demo/cu", HttpMethod.POST, body, Demo.class);
			Demo d = response.getBody();
			if(d!=null) {
				return d;
			}else {
				return new Demo();
			}
		}catch(HttpStatusCodeException e) {
			System.out.println("Codigo: " + e.getStatusCode());
			System.out.println("Error: " + e.getMessage());
			e.printStackTrace();
			return null;	
		}
	}

	@Override
	public Demo update(Demo demo) {
		try {
			HttpEntity<Demo> body = new HttpEntity<Demo>(demo);
			ResponseEntity<Demo> response = clienteRest.exchange(server + "/api/v1/demoServ/demo/cu", HttpMethod.PUT, body, Demo.class);
			Demo d = response.getBody();
			if(d!=null) {
				return d;
			}else {
				return new Demo();
			}
		}catch(HttpStatusCodeException e) {
			System.out.println("Codigo: " + e.getStatusCode());
			System.out.println("Error: " + e.getMessage());
			e.printStackTrace();
			return null;	
		}
	}

	@Override
	public String delete(Integer id) {
		try {
			Map<String, Integer> variableMap = new HashMap<String, Integer>();
			variableMap.put("id", id);
			Demo demo = clienteRest.getForObject(server + "/api/v1/demoServ/demo/get/{id}", Demo.class, variableMap);
			if(demo!=null) {
				return "OK";
			}else {
				return "";
			}
		}catch(HttpStatusCodeException  e) {
			System.out.println("Codigo: " + e.getStatusCode());
			System.out.println("Error: " + e.getMessage());
			e.printStackTrace();
			return null;	
		}
	}

}