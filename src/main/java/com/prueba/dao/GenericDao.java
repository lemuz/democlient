package com.prueba.dao;

//import java.util.List;

public interface GenericDao<CLASS, ID> {
	
//	List<CLASS> All();
	
	CLASS get(ID id);
	
	CLASS save(CLASS object);
	
	CLASS update(CLASS object);
	
	String delete(ID id);

}