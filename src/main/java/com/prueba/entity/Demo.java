package com.prueba.entity;

import java.io.Serializable;

public class Demo implements Serializable {
	
	private Integer idDemo;
	
	private String nombreDemo;

	@Override
	public String toString() {
		return "Demo [idDemo=" + idDemo + ", nombreDemo=" + nombreDemo + "]";
	}

	public Demo(Integer idDemo, String nombreDemo) {
		super();
		this.idDemo = idDemo;
		this.nombreDemo = nombreDemo;
	}

	public Demo() {
		super();
	}
	
	public Integer getIdDemo() {
		return idDemo;
	}

	public void setIdDemo(Integer idDemo) {
		this.idDemo = idDemo;
	}

	public String getNombreDemo() {
		return nombreDemo;
	}

	public void setNombreDemo(String nombreDemo) {
		this.nombreDemo = nombreDemo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idDemo == null) ? 0 : idDemo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Demo other = (Demo) obj;
		if (idDemo == null) {
			if (other.idDemo != null)
				return false;
		} else if (!idDemo.equals(other.idDemo))
			return false;
		return true;
	}

}
