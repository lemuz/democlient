package com.prueba.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.prueba.entity.Demo;
import com.prueba.service.DemoService;

@Controller
@RequestMapping("demo-client")
public class DemoController {

	final Logger logger = LoggerFactory.getLogger(DemoController.class);

	@Autowired
	DemoService demoService;

	private ModelAndView mav;

	@GetMapping(path = { "/", "/index", "/home", "/main" })
	public String index() {
		return "redirect:/homepage";
	}

	@GetMapping(value = "homepage")
	public ModelAndView home() {
		ModelAndView mv = (mav != null) ? mav : new ModelAndView();
		mv.setViewName("index");
		return mav;
	}

	@GetMapping(value = "search")
	public ModelAndView find(@RequestParam Integer idDemo) {
		ModelAndView mv = (mav != null) ? mav : new ModelAndView();
		try {
			Demo demo = demoService.get(idDemo);
			if (demo != null) {
				mv.addObject("error", false);
			} else {
				mv.addObject("error", true);
			}
			mv.addObject("demo", demo);
			mv.setViewName("demo");
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
			e.printStackTrace();
			mv.setViewName("index");
		}
		return mv;
	}
	
	@PostMapping(value = "create")
	public ModelAndView create(@ModelAttribute Demo newDemo) {
		ModelAndView mv = (mav != null) ? mav : new ModelAndView();
		try {
			Demo d = demoService.save(newDemo);
			if (d != null) {
				mv.addObject("demo-frontend", newDemo);
			} else {
				mv.addObject("error", true);
			}
			mv.setViewName("demo");
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
			e.printStackTrace();
			mv.setViewName("index");
		}
		return mv;
	}
	
	@GetMapping(value = "new")
	public ModelAndView newDemo() {
		ModelAndView mv = (mav != null)? mav : new ModelAndView();
		try {
			mv.setViewName("new");
			return mv;
		}catch(Exception e) {
			System.out.println("Error: " + e.getMessage());
			e.printStackTrace();
			logger.info(mv.toString());
			return new ModelAndView();
		}
	}
	
	@PostMapping(value = "update")
	public ModelAndView update(@ModelAttribute Demo demo) {
		ModelAndView mv = (mav != null) ? mav : new ModelAndView();
		try {
			Demo d = demoService.update(demo);
			if (d != null) {
				mv.addObject("demo-frontend", demo);
			} else {
				mv.addObject("error", true);
			}
			mv.setViewName("demo");
			return mv;
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
			e.printStackTrace();
			mv.setViewName("index");
			return mv;
		}
	}
	
	@GetMapping(value = "modify")
	public ModelAndView modificar(@ModelAttribute Demo demoU) {
		ModelAndView mv = (mav != null) ? mav : new ModelAndView();
		try {
			if (demoU != null) {
				mv.addObject("demo", demoU);
			} else {
				mv.addObject("error", true);
			}
			mv.setViewName("update");
			return mv;
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
			e.printStackTrace();
			mv.setViewName("index");
			return mv;
		}
	}
	
	@GetMapping(value = "del/{idDemo}")
	public ModelAndView delete(@PathVariable Integer idDemo) {
		ModelAndView mv = (mav != null) ? mav : new ModelAndView();
		try {
			String delete = demoService.delete(idDemo);
			if (delete.equals("")) {
				mv.addObject("demo-deleted", false);
			} else if(delete.equals("OK")){
				mv.addObject("demo-deleted", true);
			}
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
			e.printStackTrace();
			mv.addObject("demo-deleted", false);
		}
		mv.setViewName("index");
		return mv;
	}
}
